# Funbridge : problèmes et suggestions

Ce projet permet de créer des bugs ou des suggestions d'amélioration pour l'application [Funbridge](https://play.funbridge.com/).  
Voir la [liste complète](https://gitlab.com/pr.fouche/funbridge-issues/-/issues).
